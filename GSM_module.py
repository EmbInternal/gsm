import serial
import orangepi.pc
import OPi.GPIO as GPIO
import os, time
import paho.mqtt.client as mqtt
import paho.mqtt.publish as publish
import Queue
import signal
import subprocess
import sys
import csv
import paho.mqtt.client as paho
from time import sleep
import json
import urllib2
from datetime import datetime
from threading import Thread
from Adafruit_MCP230xx import Adafruit_MCP230XX

# SMS port, baudrate and timeout setup
GSM_port = serial.Serial("/dev/ttyS2", baudrate=115200, timeout=1)
gpio_DO = Adafruit_MCP230XX(address = 0x20, num_gpios = 8)

# Log Output file    
LOG_PATH = "/root/codes/python_codes/common/app_net_connect_manager/EM4G_25-08-2021_Log.csv"
with open(LOG_PATH,'a') as f:
    f.writelines("******,******,******,******,\n")
    f.close()

# VARIABLES
restarts = 0

# FUNCTIONS
class terminate_program:
    terminate_now = False
    def __init__(self):
        signal.signal(signal.SIGINT, self.terminate)
        signal.signal(signal.SIGTERM, self.terminate)
    def terminate(self, signum, frame):
        self.terminate_now = True

terminator = terminate_program()

def SIM_restart():
    global restarts
    log_data("SIM restarted")
    GPIO.output(33, 1)
    time.sleep(3)
    GPIO.output(33, 0)
    time.sleep(1)
    GPIO.output(33, 1)
    time.sleep(0.5)
    GPIO.output(33, 0)
    time.sleep(5)
    restarts = restarts + 1

def check_CPIN_resp():
    GSM_port.close()
    time.sleep(0.1)
    GSM_port.open()
    time.sleep(0.1)
    GSM_port.reset_output_buffer()
    GSM_port.write('AT+CPIN?'+'\r\n')
    time.sleep(0.1)
    rcv = GSM_port.read_until('READY'.encode('utf-8'))
    # rcv = GSM_port.read(100)
    recevd = str(rcv)
    recevd = recevd.replace("\n","")
    time.sleep(0.1)
    GSM_port.close()
    time.sleep(0.1)
    print("CPIN response: " + rcv)
    return recevd.find('READY')

def check_CSQ():
    GSM_port.open()
    time.sleep(0.5)
    GSM_port.reset_output_buffer()
    GSM_port.write(b"AT+CSQ\r")
    time.sleep(0.5)
    rcv = GSM_port.read_until('OK'.encode('utf-8'))
    rcv = rcv.replace("AT+CSQ\r","")
    recevd = str(rcv)
    result = recevd.find(',')
    sub_text = recevd[result-2: result]
    result2 = recevd.find('+CSQ:')
    sub_text2 = recevd[result2 + 6: result2 + 8]
        
    if (sub_text == sub_text2):
        csq_num = str(sub_text)
    elif recevd == "" :
        csq_resp == ""
    else: 
        csq_num = 99
    print("CSQ Response: " + csq_num)
    return csq_num

def do_poff():
    try:
        print subprocess.check_output(['/usr/bin/poff', 'provider'])
        log_data("trying to switch OFF PP0...PPP is now switched OFF")
        time.sleep(5)
    except:
        log_data("trying to switch OFF PP0...unable to switch OFF PPPD...PPP is already switched OFF")

def do_pon():
    try:
        print subprocess.check_output(['/usr/bin/pon', 'provider'])
        log_data("trying to switch ON PP0...PPP is now switched ON")
        time.sleep(20)
    except:
        log_data("trying to switch ON PP0...unable to switch ON PPPD...PPP is already switched OFF")

def check_ppp():
    data = subprocess.Popen(['ifconfig'], stdout = subprocess.PIPE)
    output = str(data.communicate())
    index = output.find('ppp0')
    if index == -1:
        status = False
        log_data("checking ppp status...PPP dosen't exist")
    else:
        status = True
        log_data("checking ppp status...PPP exist")
    return status

def check_Internet():
    try:
        response=urllib2.urlopen('http://www.google.co.in',timeout=20)
        net_status = True
        log_data("checking Internet Connectivity...Internet CONNECTED")
    except urllib2.URLError as err: 
        net_status = False
        log_data("checking Internet Connectivity...Internet DISCONNECTED")
    return net_status

def log_data(data):
    data = data.replace(",",":")
    data = data.replace("\n","")
    data = data.replace("\r","")
    with open(LOG_PATH,'a') as f:
        f.writelines(str((datetime.now()).strftime("%d/%m/%Y, %H:%M:%S")) + ", " + data + '\n')

def mqtt_conn_send(mqtt_var):
    broker="solar.embedos.io"
    port=1883
    p = paho.Client("c1")
    p.connect(broker,port)
    p.publish("EM4G_test/net_status", mqtt_var)
    log_data("connected to mqtt and sending data: " + str(mqtt_var))

## This is version 1
# def SMS_send():
#     #Logging Signal Quality
#     global restarts
    
#     while True:
#         date_time = (datetime.now()).strftime("%d/%m/%Y, %H:%M:%S")
#         do_pon()
#         ppp_status_before = check_ppp()
#         net_status_before = check_Internet()
#         if net_status_before == "Internet Active":
#             try:
#                 broker="solar.embedos.io"
#                 port=1883
#                 p = paho.Client("c1")
#                 p.connect(broker,port)
#                 print("connected to mqtt")
#                 mqtt_status_before = "mqtt connected"
#                 mqtt_status_flag = 1
#             except:
#                 print("mqtt disconnected")
#                 mqtt_status_before = "mqtt not connected"
#                 mqtt_status_flag = 0
#             if mqtt_status_flag == 1:
#                 p.publish("EM4G_test/net_status", 1)
#                 time.sleep(1)
#                 p.publish("EM4G_test/net_status", 0)
#                 time.sleep(1)
#         do_poff()
#         ppp_status_after = check_ppp()
#         net_status_after = check_Internet()
#         cpin_status = check_CPIN()
#         net_quality = check_CSQ()
#         try:
#             broker="solar.embedos.io"
#             port=1883
#             p = paho.Client("c1")
#             p.connect(broker,port)
#             print("connected to mqtt")
#             mqtt_status_after = "mqtt connected"
#             mqtt_status_flag = 1
#         except:
#             print("mqtt disconnected")
#             mqtt_status_after = "mqtt not connected"
#             mqtt_status_flag = 0

#         #Logging Internet Status
#         with open("/root/codes/python_codes/common/app_net_connect_manager/SignalQuality_Log.csv",'a') as f:
#             f.writelines( str(date_time) + " , " + cpin_status + " , " +  net_quality + " , " + str(restarts) + " , " + ppp_status_before + " , " + net_status_before + " , " + mqtt_status_before + " , " + ppp_status_after + " , " + net_status_after + " , " + mqtt_status_after + '\n')

# This is version 2
# def SMS_send():
#     global restarts
#     mqtt_data = 0
#     while True:
#         date_time = (datetime.now()).strftime("%d/%m/%Y, %H:%M:%S")
#         ppp_status = check_ppp()
#         if ppp_status == "ppp0 Inactive":
#             net_status = "NA"
#             do_poff()
#             try:
#                 cpin_resp = check_CPIN_resp()
#             except Exception as e:
#                 cpin_resp = ""
#                 error_msg = str(e)
#                 error_msg = error_msg.replace(",",":")
#                 error_msg = error_msg.replace("\n","")
#                 error_msg = error_msg.replace("\r","")
#                 with open("/root/codes/python_codes/common/app_net_connect_manager/EM4G_17-08-2021_Log.csv",'a') as f:
#                     f.writelines(str(date_time) + "Error occured:" + "," + error_msg + '\n')            
#             try:
#                 csq_resp = check_CSQ()
#             except Exception as e:
#                 csq_resp = ""
#                 error_msg = str(e)
#                 error_msg = error_msg.replace(",",":")
#                 error_msg = error_msg.replace("\n","")
#                 error_msg = error_msg.replace("\r","")
#                 with open("/root/codes/python_codes/common/app_net_connect_manager/EM4G_17-08-2021_Log.csv",'a') as f:
#                     f.writelines(str(date_time) + "," + "Error occured:" + "," + error_msg + '\n')
#             if cpin_resp == "":
#                 cpin_status = "NOT READY"
#                 csq_status = "NA"
#                 SIM_restart() 
#                 restart_reason = "no response from cpin"
#                 do_pon()
#             elif cpin_resp.find('READY') == -1:
#                 cpin_status = "NOT READY"
#                 csq_status = "NA"
#                 SIM_restart()
#                 restart_reason = "invalid response from cpin"
#                 do_pon()
#             elif csq_resp == "NA":
#                 cpin_status = "READY"
#                 csq_status = "NOT OKAY" + (str(csq_resp)).replace(",","")
#                 SIM_restart()
#                 restart_reason = "no response from csq"
#                 do_pon()
#             else:
#                 csq_status = "OKAY"
#                 cpin_status = csq_resp
#                 do_poff()
#                 do_pon()
#                 restart_reason = "no restart"
                
#         elif ppp_status == "ppp0 Active":
#             cpin_status = "OK"
#             csq_status = "OK"
#             restart_reason = "no restart"
#             net_status = check_Internet()
#             if net_status == "Internet Inactive":
#                 do_poff()
#                 time.sleep(5)
#                 do_pon()
#                 if (check_ppp() == "ppp0 Inactive" or check_Internet() ==  "Internet Inactive"):
#                     SIM_restart()
#                     restart_reason = "inactive ppp"
#                     if check_ppp() == "ppp0 Inactive":
#                         do_pon()
#             elif net_status == "Internet Active":
#                 broker="solar.embedos.io"
#                 port=1883
#                 p = paho.Client("c1")
#                 p.connect(broker,port)
#                 p.publish("EM4G_test/net_status", int(mqtt_data))
#                 mqtt_data = not mqtt_data

def EM4G_diagnosis(mqtt_data):
    global restarts
    log_data("starting Iteration")
    try:
        if check_ppp() == True:
            if check_Internet() == True:
                try:
                    mqtt_conn_send(int(mqtt_data))
                    mqtt_data = not mqtt_data
                except Exception as e:
                    log_data(str(e))
            else:
                do_poff()
                check_ppp()
                time.sleep(5)
                log_data("waiting for 5 seconds")                
                for i in range(3): 
                    log_data("trying to do PON " + str(i+1) + " times")
                    do_pon()
                    log_data("waiting for 5 seconds")
                    time.sleep(5)
                    if check_Internet() == True:
                        break
                    if i == 2:
                        SIM_restart()
                        do_poff()
                        check_ppp()
                        do_pon()
                        check_ppp()
                try:
                    mqtt_conn_send(int(mqtt_data))
                    mqtt_data = not mqtt_data
                except Exception as e:
                    log_data("ERROR: " + str(e))                    
        else:
            do_poff()
            check_ppp()
            try:
                cpin_resp = check_CPIN_resp()
            except Exception as e:
                cpin_resp = 2
                log_data("ERROR: " + str(e))
            try:
                csq_resp = check_CSQ()
            except Exception as e:
                csq_resp = 0
                log_data("ERROR: " + str(e))
            if cpin_resp == -1:
                log_data("CPIN read...SIM not ready")
                SIM_restart()
                do_pon()
            elif cpin_resp == 0:
                log_data("CPIN read...no response from CPIN")
                SIM_restart()
                do_pon()
            elif cpin_resp == 2:
                log_data("CPIN read...error while reading CPIN")
                SIM_restart()
                do_pon()
            elif csq_resp == 0:
                log_data("CSQ read...error while reading CSQ")
                SIM_restart()
                do_pon()
            elif csq_resp == 99:
                log_data("CSQ read...CSQ returned 99")
                SIM_restart()
                do_pon()
            else:
                do_pon()
        check_ppp()
        log_data("waiting for 60 seconds")
        log_data("------,------,-------------------------------------------------")
        time.sleep(60)
    except Exception as e:
        log_data("ERROR: " + str(e))
    return mqtt_data
            
def main():
    #GSM Initialization
    log_data("Starting the code")
    send_data = 0
    BOARD = orangepi.pc.BOARD
    GPIO.setmode(BOARD)
    GPIO.setup(33, GPIO.OUT)
    try:
        do_poff()    
        if check_CPIN_resp() == -1:
            SIM_restart()
        do_pon()
        check_ppp()
    except Exception as e:
        log_data("ERROR: " + str(e))
    log_data("Starting code loop!")

    while not terminator.terminate_now:
        send_data = EM4G_diagnosis(send_data)
    
main()
